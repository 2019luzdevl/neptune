#!/usr/bin/env python

import rospy

from behavior_script import Behavior

if __name__ == '__main__':     # This is the main thread, thread #1
    rospy.init_node('behavior_1')

    my_node = Behavior('behavior_1')
   
    rospy.spin()
