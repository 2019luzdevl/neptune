#!/usr/bin/env python

import rospy

# import the message type you need (here are examples)
from behavior.msg import BehaviorStatus

# If you need to protect a variable by a mutex...
from multiprocessing import Lock

class Behavior:

    def __init__(self, name): # We are in thread #1 here.
        self.name = name
        self.status = False
        self.status_mutex = Lock()
        self.sub = rospy.Subscriber('behavior', BehaviorStatus, self.sub_callback)
        self.pub = rospy.Publisher('behaviors_status', BehaviorStatus, queue_size=10)

    def sub_callback(self, msg):
        if msg.name == self.name:
            with self.status_mutex:
                copy = self.status
                self.status = msg.status
            if (copy == False) and (msg.status == True): self.on_status_on()
            if (copy == True) and (msg.status == False): self.on_status_off()
        elif msg.name == 'ping' and (not rospy.is_shutdown()):
            self.pub.publish(self.name, self.get_status())

    def on_status_on(self):
        pass

    def on_status_off(self):
        pass

    def set_status(self, status):
        self.status = status

    def get_status(self):
        with self.status_mutex:
            copy = self.status
        return copy