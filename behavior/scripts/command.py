#!/usr/bin/env python

import rospy

from std_msgs.msg import String
from behavior.msg import BehaviorStatus

behaviors = ['TakeOff', 'Land', 'Hover']
commands = {'TakeOff': [(0, 'TakeOff'), (0, 'Hover')],
            'Land': [(0, 'Hover'), (0.3, 'Land')],
            'Hover': [(0, 'Hover')]}


def deactivate_behaviors(pub):
    for behavior in behaviors:
        pub.publish(behavior, False)

class Scheduler:
    def __init__(self):
        self.sub = rospy.Subscriber("command", String, self.sub_callback)
        self.pub = rospy.Publisher('behavior', BehaviorStatus, queue_size=10)

    def sub_callback(self, command):
        deactivate_behaviors(self.pub)
        command_behaviors = commands[command.data]
        for delay, behavior in command_behaviors:
            rospy.sleep(delay)
            self.pub.publish(behavior, True)
            


if __name__ == '__main__':
    rospy.init_node('commander')
    commander = Scheduler()
    rospy.spin()