#!/usr/bin/env python

import rospy

from behavior_script import Behavior
from std_msgs.msg import Empty

class Hover(Behavior):
    def __init__(self):
        Behavior.__init__(self, 'Hover')
        self.pub_hover = rospy.Publisher('hover', Empty, queue_size=10)

    def on_status_on(self):
        self.pub_hover.publish()
        rospy.sleep(2)
        Behavior.set_status(self, False)


if __name__ == '__main__':
    rospy.init_node('Hover')
    my_node = Hover()
    rospy.spin()