#!/usr/bin/env python

import rospy

from behavior_script import Behavior
from std_msgs.msg import Empty

class Land(Behavior):
    def __init__(self):
        Behavior.__init__(self, 'Land')
        self.pub_land = rospy.Publisher('land', Empty, queue_size=10)

    def on_status_on(self):
        self.pub_land.publish()
        rospy.sleep(2)
        Behavior.set_status(self, False)


if __name__ == '__main__':
    rospy.init_node('Land')
    my_node = Land()
    rospy.spin()