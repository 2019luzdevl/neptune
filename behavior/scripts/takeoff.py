#!/usr/bin/env python

import rospy

from behavior_script import Behavior
from std_msgs.msg import Empty

class TakeOff(Behavior):
    def __init__(self):
        Behavior.__init__(self, 'TakeOff')
        self.pub_takeoff = rospy.Publisher('takeoff', Empty, queue_size=10)

    def on_status_on(self):
        self.pub_takeoff.publish()
        rospy.sleep(2)
        Behavior.set_status(self, False)


if __name__ == '__main__':
    rospy.init_node('TakeOff')
    my_node = TakeOff()
    rospy.spin()