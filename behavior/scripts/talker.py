#!/usr/bin/env python


import rospy
from behavior.msg import BehaviorStatus

def talker():
    pub = rospy.Publisher('behavior', BehaviorStatus, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(0.2) # 10hz
    msg = BehaviorStatus()
    msg.name = 'behavior_1'
    msg.status = True
    while not rospy.is_shutdown():
        msg.status = not msg.status
        pub.publish(msg)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
