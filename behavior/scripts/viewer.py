#!/usr/bin/env python

import rospy
from sensor_msgs.msg import CompressedImage
import numpy as np
import cv2



class MyNode:
    def __init__(self):
        self.sub_image = rospy.Subscriber("/image_out/compressed", CompressedImage,
                                          self.on_image,
                                          queue_size=1,
                                          buff_size=2 ** 22)
        cv2.namedWindow('My window')

    def __del__(self):
        cv2.destroyAllWindows()

    def on_image(self, msg):
        compressed_in = np.fromstring(msg.data, np.uint8)
        frame = cv2.imdecode(compressed_in, cv2.IMREAD_COLOR)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        cv2.imshow('My window', gray)
        cv2.waitKey(1)


if __name__ == '__main__':
    rospy.init_node('my_node')
    my_node = MyNode()
    rospy.spin()