      #!/usr/bin/env python

import rospy
from std_msgs.msg import Empty, Float32
from st5_generic.utils import get_param
from geometry_msgs.msg import Twist, Vector3
from nav_msgs.msg import Odometry
from multiprocessing import Lock
import numpy as np
import scipy


def twist2array(twist):
    arr = np.array([[twist.linear.x, twist.linear.y, twist.linear.z], [.0, .0, twist.angular.z]])
    return arr

def array2twist(arr):
    twist = Twist()
    twist.linear.x = arr[0,0]
    twist.linear.y = arr[0,1]
    twist.linear.z = arr[0,2]
    twist.angular.x = .0
    twist.angular.y = .0
    twist.angular.z = arr[1,2]
    return twist


hover_vel = np.array([[.0,.0,.0],[.0,.0,.0]]).copy
error_o = np.array([.0, .0]).copy

class SpeedController:

    def __init__(self):

        # Publishers
        self.pub_twist = rospy.Publisher('/bebop/cmd_vel', Twist, queue_size=1, latch=True)

        # Subscribers
        
        self.sub_twist = rospy.Subscriber('/target_vel', Twist, self.vel_ctrl, queue_size=1)
        self.hover_sub = rospy.Subscriber('/hover', Empty, self.hover_mode, queue_size=1)
        self.sub_odom = rospy.Subscriber('/bebop/odom', Odometry, self.on_odom, queue_size=1)

        self.target_vel = hover_vel()
        self.mutex = Lock()
        self.hover = False
        self.last_odom = hover_vel()
        self.last_odom_mutex = Lock()

        # Create the pid state
        self.state_pid = {'error':error_o(), 'd_error':error_o(), 'i_error':error_o(), 'time':rospy.Time.now()}
        self.coef_pid = {'p':np.array([1.4, 0.8]), 'd':np.array([0.79, 0.41]), 'i':np.array([1.64, 1.03])}

        # pid mode
        if get_param('/tuning', True):
            print('ok')
            axis = get_param('/tuning_axis', 'x')
            self.tuning_axis = 1 if axis == 'y' else 0
            self.coef_pid = {'p':error_o(), 'd':error_o(), 'i':error_o()}
            self.sub_pid = rospy.Subscriber('/P_tuning', Float32, self.pid_tuning, queue_size=1)

    
    def vel_ctrl(self, msg):
        with self.mutex:
            self.target_vel = twist2array(msg)
            self.target_vel[1,0:2] = (.0, .0)
        self.pub_vel()
    
    def hover_mode(self, msg):
        rospy.loginfo("[Speed_Controller] Hover mode " + (self.hover * "dis") + "engaged")
        self.state_pid = {'error':error_o(), 'd_error':error_o(), 'i_error':error_o(), 'time':rospy.Time.now()}
        self.hover= not self.hover
        self.pub_vel()

    def pid_tuning(self, msg):
        self.coef_pid['p'][self.tuning_axis] = msg.data
        rospy.loginfo('[pid] P coeficient at '+str(msg.data)+'.')
        self.pub_vel()

    def on_odom(self, msg):
        with self.last_odom_mutex:
            self.last_odom = twist2array(msg.twist.twist)
            self.last_odom[1,0:2] = (.0, .0)
        self.pub_vel()

    def pub_vel(self):
        if self.hover:
            self.pub_twist.publish(array2twist(hover_vel()))
            return
        with self.last_odom_mutex:
            state = self.last_odom.copy()
        with self.mutex:
            command = self.target_vel.copy()
        command[0, 0:2] = self.pid(command[0, 0:2] - state[0, 0:2])
        command[0, 0] = min(max(-1.0, command[0, 0]), 1.0)
        command[0, 1] = min(max(-1.0, command[0, 1]), 1.0)
        self.pub_twist.publish(array2twist(command))

    def pid(self, error):
        now = rospy.Time.now()
        dt = (now - self.state_pid['time']).to_sec()
        self.state_pid['time'] = now
        self.state_pid['d_error'] = (error - self.state_pid['error']) / dt
        self.state_pid['error'] = error
        self.state_pid['i_error'] += error * dt
        return (self.state_pid['error'] * self.coef_pid['p'] + self.state_pid['d_error'] * self.coef_pid['d'] + self.state_pid['i_error'] * self.coef_pid['i'])

if __name__ == '__main__':
    rospy.init_node('Speed_controller')
    Speed_controller = SpeedController()
    rospy.spin()