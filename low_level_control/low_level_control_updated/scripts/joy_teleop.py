#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist, Vector3
from sensor_msgs.msg import Joy
from std_msgs.msg import Empty, Float32
from st5_generic.utils import get_param

class JoyTeleop:

    def __init__(self):
        # Parameters
        self.angle_limiter = get_param('~angle_limit', 0.5)

        # Internal state
        self.on = False
        self.hover_on = False
        self.hover_change = False
        self.log_P = -5
        self.previous_Dv = .0
        self.previous_Dh = .0
        self.previous_R = False

        # Publishers
        self.pub_twist = rospy.Publisher('/target_vel', Twist, queue_size=1, latch= True)
        self.to_pub = rospy.Publisher('/bebop/takeoff', Empty, queue_size=1, latch= True)
        self.l_pub =  rospy.Publisher('/bebop/land', Empty, queue_size=1, latch= True)
        self.hover_pub = rospy.Publisher('/hover', Empty, queue_size=1)
        self.pid_pub = rospy.Publisher('/P_tuning', Float32, queue_size=1)

        # Subscribers
        self.joystick_sub =rospy.Subscriber('/joy', Joy, self.teleop_joy)

        # Axis choice
        self.tuned_axis = 1 if get_param('/tuning_axis', 'x')=='y' else 0

    def teleop_joy(self, data):

        # Unpacking data
        L_press,R_press = data.buttons[4:6]
        y,x = (data.axes[0], .0) if self.tuned_axis else (.0, data.axes[1])
        Rz,z = data.axes[3:5]

        # Creating Twist vectors
        linear = Vector3(x*self.angle_limiter, y*self.angle_limiter, z)
        angular = Vector3(.0, .0, Rz)

        #  Taking Off
        if L_press:
            if not self.on:
                self.to_pub.publish()
                self.on = True
                rospy.loginfo('[Teleop_joy] Taking off.')

        # Pub Twist
            self.pub_twist.publish(linear, angular)

        #Landing 
        elif self.on:
            self.l_pub.publish()
            self.on = False
            self.pub_twist.publish(Vector3(.0,.0,.0), Vector3(.0,.0,.0))
            rospy.loginfo('[Teleop_joy] Landing.')

        # Hover mode
        if R_press and not self.previous_R:
            self.hover_pub.publish()
        self.previous_R = bool(R_press)
            

        # Updating pid
        dP = data.axes[-1]
        if self.previous_Dv<0.5:
            self.log_P += data.axes[-1]/10
        if self.previous_Dh<0.5:
            self.log_P -= data.axes[-2]/100
        self.previous_Dv, self.previous_Dh = abs(data.axes[-1]), abs(data.axes[-2])
        P = 10**self.log_P
        self.pid_pub.publish(P)

        return

    def run(self):
        rospy.spin()

def main():
    rospy.init_node('joy_teleop')
    joy_teleop = JoyTeleop()
    joy_teleop.run()

if __name__ == '__main__':
    main()
