import numpy as np
import cv2 as cv

#lines = (N,8), lines[0] = (x1;y1;x2;y2;a;b;c;n)
def segments(img, lines, color, thickness) :
	for line in lines:
		cv.line(img, (int(line[0]),int(line[1])), (int(line[2]),int(line[3])), color, thickness)


def lines(img, lines, color, thickness) :
	for line in lines:
		cv.line(img, (int(line[0]),int(line[1])), (int(line[2]),int(line[3])), color, thickness)


def curve(img, values, hmin, hmax, ymin, ymax, color, thickness) :
#values are in the range [0,255]
	x=1
	reference = max(values)/255
	for i in range(len(values)) : #len(values) = width of the image
		if i==0 :
			i=1	#just to avoid the problem of the initial condition
		cv.line(img, (x-1, int(values[i-1]/reference)), (x, int(values[i]/reference)), color, thickness)





