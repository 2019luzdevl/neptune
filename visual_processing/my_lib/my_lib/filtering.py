import numpy as np
import cv2 as cv

def longer_than(lines, min_length) :
	new_lines = []
	for line in lines:
		if line[7]>min_length:
			new_lines.append(line)
	return new_lines

def not_vertical(lines) :
	new_lines = []
	for line in lines:
		if abs(line[5])<=0.01:	#if b~0, the line is vertical
			new_lines.append(line)
	return new_lines

def outside(img, points) :
	width = img.shape[1]
	height = img.shape[0]
	print('height: ', height)
	print('width: ', width)
	points = points.tolist()
	for point in points:
		if (point[0]<0 or point[0]>width or point[1]<0 or point[1]>height):
			points.remove(point)
	points = np.array(points)

	return points

def distance_line_point(line, point) :
	v1 = [(line[2]-line[0] + 0.0), (line[3]-line[1])] #vector that describes the line
	v2 = [(line[2]-point[0] + 0.0), (line[3]-point[1] + 0.0)] #vector which we'll use to calculate the cross product
	distance = np.linalg.norm(np.cross(v1,v2))/np.linalg.norm(v1)
	return distance

