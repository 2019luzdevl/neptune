import cv2 as cv
import numpy as np

def line_detection(img):
	sdfg = cv.createLineSegmentDetector()
	lines, _, _, _ = sdfg.detect(img)
	new_lines = []
	for line in lines:
		x=line[0][2]-line[0][0] + 0.001 #to make sure it's a float value
		y=line[0][3]-line[0][1] + 0.001
		b = np.sqrt(1/(1+(y**2)/(x**2))) #to make sure b>0 and b^2 + a^2 = 1
		a = -b*y/x
		c = -(a*line[0][0] + b*line[0][1])
		n = np.sqrt(x**2+y**2)
		dfgh = np.append(line,a)
		dfgh = np.append(dfgh,b)
		dfgh = np.append(dfgh,c)
		dfgh = np.append(dfgh,n)
		new_lines.append(dfgh)
	return new_lines
