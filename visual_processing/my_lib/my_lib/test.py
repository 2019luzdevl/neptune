import cv2 as cv
import numpy as np
import vp_detection
import filtering

def on_value_changed(v) :
    print(v)


cap = cv.VideoCapture(0)
img = cap.read()[1]
img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

##start of the test:
#lines = line_detection.line_detection(img)
#drawing.lines(img, lines, (255,0,0),2) #any color that contrast with the environment

vp = vp_detection.vp_detection(img)

#end of the test

cap.release()	
cv.namedWindow('My window') # creates a window
cv.createTrackbar('value', 'My window', 0, 1000,  on_value_changed)

keycode = 0
delay   = 1          # ms, 0 means 'bock until the user hits a key'
while keycode != 27: # 27 means ESC in ascii.
    cv.imshow('My window', img)
    keycode = cv.waitKey(delay) & 0xFF
    if keycode == ord('x') :
	print('the x key is pressed.')
cv.destroyAllWindows()
