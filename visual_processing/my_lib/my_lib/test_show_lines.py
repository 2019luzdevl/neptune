import cv2 as cv
import line_detection
import drawing
import filtering

def on_value_changed(v) :
    print(v)

def test_show_lines() :
	cap = cv.VideoCapture(0)
	img = cap.read()[1]

	img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
	lines = line_detection.line_detection(img)
	drawing.lines(img, lines, (255,0,0),2) #any color that contrast with the environment

	cap.release()	
	cv.namedWindow('My window') # creates a window
	cv.createTrackbar('value', 'My window', 0, 1000,  on_value_changed)

	keycode = 0
	delay   = 1          # ms, 0 means 'bock until the user hits a key'
	while keycode != 27: # 27 means ESC in ascii.
	    cv.imshow('My window', img)
	    keycode = cv.waitKey(delay) & 0xFF
	    if keycode == ord('x') :
		print('the x key is pressed.')
	cv.destroyAllWindows()

test_show_lines()
print('one step at its time')
