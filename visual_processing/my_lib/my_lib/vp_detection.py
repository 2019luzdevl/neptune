import numpy as np
import cv2 as cv
import drawing
import intersect
import line_detection
import filtering
import sys

def sci(values, confidence) :
	#"""
	#values : an array of scalars (e.g. [1.2, 100.6, 23.78, ....])
	#confidence : in [0,1], (e.g 0.5 for 50%)
	#"""
	nb        = values.shape[0]
	values    = np.sort(values)
	size      = (int)(nb*confidence+.5)
	nb_iter   = nb - size + 1
	sci       = None
	sci_width = sys.float_info.max
	inf       = 0
	sup       = size 
	for i in range(nb_iter) :
		sciw = values[sup-1] - values[inf]
		if sciw < sci_width :
			sci       = values[inf:sup]
			sci_width = sciw
		inf += 1
		sup += 1
	# The result is the array (ordered) of the values inside the sci.
	return sci

def vp_detection(img) :
	lines = filtering.not_vertical(line_detection.line_detection(img))
	values = intersect.intersect(lines)	#intersections
	values = filtering.outside(img,values)
	print('working...')
	delta_x = sci(values[:, 0],0.5)	#confidence = 50%
	delta_y = sci(values[:, 1],0.5)
	if (delta_x==None or delta_y==None) :
		print('it found no vanishing point')
	else:
		vp = [np.mean(delta_x), np.mean(delta_y)]
		print('vp: ')
		print(vp)
		cv.circle(img, (int(vp[0]),int(vp[1])),1, (0,0,255), -1)	#showing the VP as a small black circle
		
		for line in lines:
			if filtering.distance_line_point(line,vp)<=5:	#showing the lines we consider at a small distance to the VP as 'converging' to it
				print('it found one!')
				cv.line(img, (int(line[0]),int(line[1])), (int(line[2]),int(line[3])),(255,0,0),2)
		return vp


