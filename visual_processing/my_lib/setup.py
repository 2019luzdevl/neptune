#!/usr/bin/env python3

from setuptools import setup

setup(name='my_lib',
      version='0.1',
      description='my library',
      url='',
      author='Arthur D.P.S. Lima',
      author_email='',
      license='MIT',
      packages=['my_lib'],
      zip_safe=False)
