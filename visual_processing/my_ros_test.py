import numpy as np
import cv2
import rospy
from my_lib import vp_detection

class test:

	def _init_(self):
		self.sub_image = rospy.Subscriber("/bebop/image_raw/compressed",  CompressedImage, self.on_image,  queue_size = 1, buff_size=2**22)
		self.lsd_image_pub = rospy.Publisher("/lsd_image/compressed",  CompressedImage, queue_size = 1)
		#self.lines_pub = rospy.Publisher("/lsd_image/lines", )
		self.confidence = rospy.get_param('~confidence', 0.25)
		self.bridge= CvBridge()

	def on_image(self, img) :
		#get image:
		img = self.bridge.compressed_imgmsg_to_cv2(img_msg, desired_encoding='passthrough')
		gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
		vp = vp_detection.vp_detection(gray_img)

		last_img = self.bridge.cv2_to_compressed_imgmsg(gray_img, dst_format='jpg')
        
        	self.lsd_image_pub.publish(last_img)


if _name_ == '_main_':
    rospy.init_node('test')
    test = test()
    rospy.spin()
